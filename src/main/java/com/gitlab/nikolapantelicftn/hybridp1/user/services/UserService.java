package com.gitlab.nikolapantelicftn.hybridp1.user.services;

import com.gitlab.nikolapantelicftn.hybridp1.user.exceptions.UserExistsException;
import com.gitlab.nikolapantelicftn.hybridp1.user.exceptions.UserNotFoundException;
import com.gitlab.nikolapantelicftn.hybridp1.user.model.User;
import com.gitlab.nikolapantelicftn.hybridp1.user.repositories.UserRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

@Service
public class UserService {

    private static final String USER_ROLE = "ROLE_USER";

    private final UserRepository repository;
    private final PasswordEncoder encoder;

    public UserService(UserRepository repository, PasswordEncoder encoder) {
        this.repository = repository;
        this.encoder = encoder;
    }

    @Transactional(readOnly = true)
    public List<User> getAll() {
        return repository.findAll();
    }

    @Transactional(readOnly = true)
    public User get(String username) {
        return repository.findByUsername(username)
                .orElseThrow(() -> new UserNotFoundException(username));
    }

    @Transactional(readOnly = true)
    public User get(Long id) {
        return repository.findById(id)
                .orElseThrow(() -> new UserNotFoundException(id));
    }

    @Transactional
    public User create(User newUser) {
        validateCreate(newUser);
        newUser.setRole(USER_ROLE);
        newUser.hashPassword(encoder::encode);
        return repository.save(newUser);
    }

    @Transactional
    public User update(Long id, User newUser) {
        validateUpdate(id, newUser);
        User old = get(id);
        String currentPassword = old.getPassword();
        newUser.setId(id);
        newUser.setRole(old.getRole());
        newUser.setPassword(currentPassword);
        return repository.save(newUser);
    }

    @Transactional
    public void updateUsername(String oldUsername, String newUsername) {
        if (repository.existsByUsername(newUsername)) {
            throw new UserExistsException(newUsername);
        }
        User found = get(oldUsername);

        found.setUsername(newUsername);
        repository.save(found);
    }

    @Transactional
    public void updatePassword(String username, String newPassword) {
        User found = get(username);
        found.setPassword(newPassword);
        found.hashPassword(encoder::encode);
        repository.save(found);
    }

    @Transactional
    public void delete(Long id) {
        if (!repository.existsById(id)) {
            throw new UserNotFoundException(id);
        }
        repository.deleteById(id);
    }

    @Transactional
    public void delete(String username) {
        if (!repository.existsByUsername(username)) {
            throw new UserNotFoundException(username);
        }
        repository.deleteByUsername(username);
    }

    private void validateUsernameUpdate(Long id, String username) {
        boolean userExists = repository.existsByUsername(username);
        User found = repository.findById(id).orElseThrow(() -> new UserNotFoundException(id));
        if (userExists && !found.getUsername().equals(username)) {
            throw new UserExistsException(username);
        }
    }

    private void validateNewUsername(String username) {
        if (repository.existsByUsername(username)) {
            throw new UserExistsException(username);
        }
    }

    private void validateUpdate(Long id, User user) {
        Objects.requireNonNull(user);
        validateUsernameUpdate(id, user.getUsername());
    }

    private void validateCreate(User user) {
        Objects.requireNonNull(user);
        validateNewUsername(user.getUsername());
    }

}
