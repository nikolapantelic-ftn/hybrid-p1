package com.gitlab.nikolapantelicftn.hybridp1.user.exceptions;

import com.gitlab.nikolapantelicftn.hybridp1.exceptions.EntityExistsException;
import com.gitlab.nikolapantelicftn.hybridp1.user.model.User;

public class UserExistsException extends EntityExistsException {

    public UserExistsException(String username) {
        super(username, "username", User.class);
    }

}
