package com.gitlab.nikolapantelicftn.hybridp1.user.exceptions;

import com.gitlab.nikolapantelicftn.hybridp1.exceptions.EntityNotFoundException;
import com.gitlab.nikolapantelicftn.hybridp1.user.model.User;

public class UserNotFoundException extends EntityNotFoundException {

    public UserNotFoundException(Long id) {
        super(id, User.class);
    }

    public UserNotFoundException(String username) {
        super(username, "username", User.class);
    }

}
