package com.gitlab.nikolapantelicftn.hybridp1.user.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gitlab.nikolapantelicftn.hybridp1.rent.model.RentedBook;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;

@Entity
public class User implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(unique = true)
    private String username;
    @NotBlank
    private String firstName;
    @NotBlank
    private String lastName;
    @NotBlank
    @JsonIgnore
    private String password;
    @ManyToOne
    private Role role;
    @OneToMany(mappedBy = "renter")
    private List<RentedBook> rentedBooks;

    public User() {}

    public User(Long id, String username, String firstName, String lastName, Role role) {
        init(id, username, firstName, lastName, null, role, null);
    }

    public User(String username, String firstName, String lastName, Role role) {
        init(null, username, firstName, lastName, null, role, null);
    }

    public User(Long id, String username, String firstName, String lastName, String password, Role role, List<RentedBook> rentedBooks) {
        init(id, username, firstName, lastName, password, role, rentedBooks);
    }

    public User(String username, String firstName, String lastName, String password, Role role) {
        init(null, username, firstName, lastName, password, role, null);
    }

    private void init(Long id, String username, String firstName, String lastName, String password, Role role, List<RentedBook> rentedBooks) {
        this.id = id;
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
        this.role = role;
        this.rentedBooks = rentedBooks;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return List.of(role);
    }

    @Override
    public String getPassword() {
        return password;
    }

    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public void setRole(String roleName) {
        this.role = new Role(roleName);
    }

    public List<RentedBook> getRentedBooks() {
        return rentedBooks;
    }

    public void setRentedBooks(List<RentedBook> rentedBooks) {
        this.rentedBooks = rentedBooks;
    }

    public void hashPassword(Function<? super String, String> hashFunction) {
        this.password = hashFunction.apply(password);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id.equals(user.id) && username.equals(user.username) && firstName.equals(user.firstName) && lastName.equals(user.lastName) && role.equals(user.role);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, username, firstName, lastName, role);
    }

}
