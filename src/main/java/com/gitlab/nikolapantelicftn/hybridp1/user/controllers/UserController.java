package com.gitlab.nikolapantelicftn.hybridp1.user.controllers;

import com.gitlab.nikolapantelicftn.hybridp1.user.controllers.dtos.NewUserDTO;
import com.gitlab.nikolapantelicftn.hybridp1.user.controllers.dtos.UserUpdateDTO;
import com.gitlab.nikolapantelicftn.hybridp1.user.controllers.dtos.UserViewDTO;
import com.gitlab.nikolapantelicftn.hybridp1.user.model.User;
import com.gitlab.nikolapantelicftn.hybridp1.user.services.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.annotation.Secured;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

@RequestMapping("api/users")
@RestController
public class UserController {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    private final UserService service;
    private final ModelMapper mapper;

    public UserController(UserService service, ModelMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @Secured("ROLE_ADMIN")
    @Operation(summary = "Get all users")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Array of users", content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, array = @ArraySchema(
                            schema = @Schema(implementation = UserViewDTO.class)
                    ))
            })
    })
    @GetMapping
    public List<UserViewDTO> getAll() {
        logger.info("GET '/api/users': getting all users");

        return service.getAll().stream()
                .map(user -> mapper.map(user, UserViewDTO.class))
                .collect(Collectors.toList());
    }

    @Secured("ROLE_ADMIN")
    @Operation(summary = "Get a user by username")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "User is found", content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = UserViewDTO.class))
            }),
            @ApiResponse(responseCode = "404", description = "User is not found", content = @Content)
    })
    @GetMapping("{username}")
    public UserViewDTO get(@PathVariable String username) {
        logger.info("GET '/api/users/{}': getting user with username {}", username, username);

        User found = service.get(username);
        return mapper.map(found, UserViewDTO.class);
    }

    @GetMapping("profile")
    public UserViewDTO get(Principal user) {
        logger.info("GET '/api/users/profile': User requesting profile info");

        User found = service.get(user.getName());
        return mapper.map(found, UserViewDTO.class);
    }


    @Secured("ROLE_ADMIN")
    @PostMapping
    public UserViewDTO create(@Valid @RequestBody NewUserDTO newUser) {
        logger.info("POST '/api/users': creating new user with username: {}", newUser.getUsername());

        User created = service.create(mapper.map(newUser, User.class));
        return mapper.map(created, UserViewDTO.class);
    }

    @Secured("ROLE_ADMIN")
    @PutMapping("{id}")
    public UserViewDTO update(@PathVariable Long id, @Valid @RequestBody UserUpdateDTO user) {
        logger.info("PUT '/api/users/{}: updating user with id: {}", id, id);

        User updated = service.update(id, mapper.map(user, User.class));
        return mapper.map(updated, UserViewDTO.class);
    }

    @Secured("ROLE_ADMIN")
    @DeleteMapping("{username}")
    public void delete(@PathVariable String username) {
        logger.info("DELETE '/api/users/{}': deleting user with username: {}", username, username);

        service.delete(username);
    }

}
