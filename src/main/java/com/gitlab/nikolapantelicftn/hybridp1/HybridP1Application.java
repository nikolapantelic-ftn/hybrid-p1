package com.gitlab.nikolapantelicftn.hybridp1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HybridP1Application {

	public static void main(String[] args) {
		SpringApplication.run(HybridP1Application.class, args);
	}

}
