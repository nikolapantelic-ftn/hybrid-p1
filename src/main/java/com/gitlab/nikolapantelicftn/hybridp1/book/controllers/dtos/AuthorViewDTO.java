package com.gitlab.nikolapantelicftn.hybridp1.book.controllers.dtos;

public class AuthorViewDTO {

    private Long id;
    private String firstName;
    private String middleName;
    private String lastName;

    public AuthorViewDTO() {}

    public AuthorViewDTO(Long id, String firstName, String middleName, String lastName) {
        init(id, firstName, middleName, lastName);
    }

    public AuthorViewDTO(String firstName, String middleName, String lastName) {
        init(null, firstName, middleName, lastName);
    }

    private void init(Long id, String firstName, String middleName, String lastName) {
        this.id = id;
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

}
