package com.gitlab.nikolapantelicftn.hybridp1.book.controllers.dtos;

import java.time.LocalDate;
import java.util.List;

public class BookViewDTO {

    private Long id;
    private String title;
    private String subtitle;
    private LocalDate creationDate;
    private String isbn;
    private List<AuthorViewDTO> authors;
    private Integer quantity;
    private String imageUrl;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public List<AuthorViewDTO> getAuthors() {
        return authors;
    }

    public void setAuthors(List<AuthorViewDTO> authors) {
        this.authors = authors;
    }

}
