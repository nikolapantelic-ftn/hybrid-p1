package com.gitlab.nikolapantelicftn.hybridp1.book.controllers;

import com.gitlab.nikolapantelicftn.hybridp1.book.controllers.dtos.AuthorViewDTO;
import com.gitlab.nikolapantelicftn.hybridp1.book.controllers.dtos.AuthorUpdateDTO;
import com.gitlab.nikolapantelicftn.hybridp1.book.controllers.dtos.NewAuthorDTO;
import com.gitlab.nikolapantelicftn.hybridp1.book.model.Author;
import com.gitlab.nikolapantelicftn.hybridp1.book.services.AuthorService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/authors")
public class AuthorController {

    private static Logger logger = LoggerFactory.getLogger(AuthorController.class);

    private final AuthorService service;
    private final ModelMapper mapper;

    public AuthorController(AuthorService service, ModelMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @Operation(summary = "Get all authors")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Array of authors", content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, array = @ArraySchema(
                            schema = @Schema(implementation = AuthorViewDTO.class)
                    ))
            }),
    })
    @Secured("ROLE_ADMIN")
    @GetMapping
    public List<AuthorViewDTO> getAll() {

        logger.info("GET '/api/authors': Fetching all authors");

        return service.getAll().stream()
                .map(author -> mapper.map(author, AuthorViewDTO.class))
                .collect(Collectors.toList());
    }

    @Operation(summary = "Get an author by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Author is found", content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = AuthorViewDTO.class))
            }),
            @ApiResponse(responseCode = "404", description = "Author is not found", content = @Content)
    })
    @Secured("ROLE_ADMIN")
    @GetMapping("{id}")
    public AuthorViewDTO get(@PathVariable Long id) {

        logger.info("GET '/api/authors/{}': Fetching author with id {}", id, id);

        Author found = service.get(id);
        return mapper.map(found, AuthorViewDTO.class);
    }

    @Operation(summary = "Create a new author")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Author successfully created", content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = AuthorViewDTO.class))
            }),
            @ApiResponse(responseCode = "400", description = "Invalid Author JSON format", content = @Content),
    })
    @Secured("ROLE_ADMIN")
    @PostMapping
    public AuthorViewDTO create(@Valid @RequestBody NewAuthorDTO author) {

        logger.info("POST '/api/authors': Creating new author");

        Author created = service.create(mapper.map(author, Author.class));
        return mapper.map(created, AuthorViewDTO.class);
    }

    @Operation(summary = "Update an existing author")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Author successfully updated", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = AuthorViewDTO.class))
            }),
            @ApiResponse(responseCode = "400", description = "Invalid Author JSON format", content = @Content),
            @ApiResponse(responseCode = "404", description = "Author is not found", content = @Content)
    })
    @Secured("ROLE_ADMIN")
    @PutMapping("{id}")
    public AuthorViewDTO update(@Valid @RequestBody AuthorUpdateDTO author, @PathVariable Long id) {

        logger.info("PUT '/api/authors/{}': Updating author with id {}", id, id);

        Author updated = service.update(id, mapper.map(author, Author.class));
        return mapper.map(updated, AuthorViewDTO.class);
    }

    @Operation(summary = "Delete an author by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Author successfully deleted", content = @Content),
            @ApiResponse(responseCode = "404", description = "Author is not found", content = @Content)
    })
    @Secured("ROLE_ADMIN")
    @DeleteMapping("{id}")
    public void delete(@PathVariable Long id) {

        logger.info("DELETE '/api/authors/{}': Deleting author with id {}", id, id);

        service.delete(id);
    }

}
