package com.gitlab.nikolapantelicftn.hybridp1.book.exceptions;

import com.gitlab.nikolapantelicftn.hybridp1.book.model.Book;
import com.gitlab.nikolapantelicftn.hybridp1.exceptions.EntityNotFoundException;

public class BookNotFoundException extends EntityNotFoundException {

    public BookNotFoundException(Long id) {
        super(id, Book.class);
    }

}
