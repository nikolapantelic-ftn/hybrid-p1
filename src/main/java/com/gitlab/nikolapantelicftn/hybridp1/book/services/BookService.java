package com.gitlab.nikolapantelicftn.hybridp1.book.services;

import com.gitlab.nikolapantelicftn.hybridp1.book.exceptions.BookNotFoundException;
import com.gitlab.nikolapantelicftn.hybridp1.book.model.Author;
import com.gitlab.nikolapantelicftn.hybridp1.book.model.Book;
import com.gitlab.nikolapantelicftn.hybridp1.book.repositories.BookRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import com.gitlab.nikolapantelicftn.hybridp1.exceptions.ApplicationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

@Service
public class BookService {

    private final BookRepository repository;
    private final AuthorService authorService;

    public BookService(BookRepository repository, AuthorService authorService) {
        this.repository = repository;
        this.authorService = authorService;
    }

    @Transactional
    public Book create(Book newBook) {
        Objects.requireNonNull(newBook);
        createAuthorsIfNotExist(newBook.getAuthors());
        return repository.save(newBook);
    }

    private void createAuthorsIfNotExist(List<Author> authors) {
        authors.forEach(author -> {
            if (!authorService.exists(author.getId())) {
                Author created = authorService.create(author);
                author.setId(created.getId());
            }
        });
    }

    @Transactional(readOnly = true)
    public List<Book> getAll() {
        return repository.findAll();
    }

    @Transactional(readOnly = true)
    public Book get(Long id) {
        return repository.findById(id)
                .orElseThrow(() -> new BookNotFoundException(id));
    }

    @Transactional(readOnly = true)
    public List<Book> get(Specification<Book> specification, String pageNum) {
        try {
            return repository.findAll(specification, PageRequest.of(Integer.parseInt(pageNum), 10, Sort.by(Sort.Direction.ASC, "title"))).stream().toList();
        } catch (NumberFormatException e) {
            return repository.findAll(specification);
        }
    }

    @Transactional
    public void delete(Long id) {
        Book found = get(id);
        if (found.hasRentedCopies()) {
            throw new ApplicationException("Book has rented copies!");
        }
        repository.deleteById(id);
    }

    @Transactional
    public Book update(Long id, Book newBook) {
        Objects.requireNonNull(newBook);
        if (!repository.existsById(id)) {
            throw new BookNotFoundException(id);
        }
        newBook.setId(id);
        return repository.save(newBook);
    }

    public List<Book> getMostRented(Integer amount) {
        return repository.getMostRented(PageRequest.of(0, amount)).stream().toList();
    }

}
