package com.gitlab.nikolapantelicftn.hybridp1.book.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.Objects;

@Entity
public class Author {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotBlank
    private String firstName;
    private String middleName;
    @NotBlank
    private String lastName;
    @ManyToMany(mappedBy = "authors")
    private List<Book> books;

    public Author() {}

    public Author(String firstName, String lastName) {
        init(null, firstName, null, lastName, null);
    }

    public Author(String firstName, String middleName, String lastName) {
        init(null, firstName, middleName, lastName, null);
    }

    public Author(Long id, String firstName, String middleName, String lastName, List<Book> books) {
        init(id, firstName, middleName, lastName, books);
    }

    private void init(Long id, String firstName, String middleName, String lastName, List<Book> books) {
        this.id = id;
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.books = books;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }

    public boolean hasBooks() {
        return !books.isEmpty();
    }

    public String getFullName() {
        StringBuilder stringBuilder = new StringBuilder(firstName + " ");
        if (middleName != null && !middleName.isEmpty())
            stringBuilder.append(middleName.charAt(0) + ". ");
        stringBuilder.append(lastName);
        return stringBuilder.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Author author = (Author) o;
        return firstName.equals(author.firstName) && Objects.equals(middleName, author.middleName) && lastName.equals(author.lastName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, middleName, lastName);
    }

}
