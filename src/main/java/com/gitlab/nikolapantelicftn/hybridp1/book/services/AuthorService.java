package com.gitlab.nikolapantelicftn.hybridp1.book.services;

import com.gitlab.nikolapantelicftn.hybridp1.book.model.Author;
import com.gitlab.nikolapantelicftn.hybridp1.book.repositories.AuthorRepository;
import com.gitlab.nikolapantelicftn.hybridp1.exceptions.ApplicationException;
import com.gitlab.nikolapantelicftn.hybridp1.exceptions.EntityNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

@Service
public class AuthorService {

    private final AuthorRepository repository;

    public AuthorService(AuthorRepository repository) {
        this.repository = repository;
    }

    @Transactional(readOnly = true)
    public Author get(Long id) {
        return repository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(id, Author.class));
    }

    @Transactional(readOnly = true)
    public List<Author> getAll() {
        return repository.findAll();
    }

    @Transactional(readOnly = true)
    public boolean exists(Long id) {
        if (id == null) {
            return false;
        }
        return repository.existsById(id);
    }

    @Transactional
    public Author create(Author author) {
        Objects.requireNonNull(author);
        return repository.save(author);
    }

    @Transactional
    public Author update(Long id, Author author) {
        Objects.requireNonNull(author);
        if (!repository.existsById(id)) {
            throw new EntityNotFoundException(id, Author.class);
        }
        author.setId(id);
        return repository.save(author);
    }

    @Transactional
    public void delete(Long id) {
        Author found = get(id);
        if (found.hasBooks()) {
            throw new ApplicationException("Author that has books cannot be deleted!");
        }
        repository.deleteById(id);
    }

}
