package com.gitlab.nikolapantelicftn.hybridp1.book.controllers.specifications;

import com.gitlab.nikolapantelicftn.hybridp1.book.model.Author;
import com.gitlab.nikolapantelicftn.hybridp1.book.model.Book;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public record BookSpecification(
        Map<String, String> queryMap) implements Specification<Book> {

    @Override
    public Predicate toPredicate(Root<Book> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        Predicate[] predicates = getPredicatesFromQueryParams(root, criteriaBuilder).toArray(new Predicate[0]);
        if (predicates.length == 0) {
            return criteriaBuilder.and();
        }
        criteriaQuery.distinct(true);
        return criteriaBuilder.or(predicates);
    }

    private List<Predicate> getPredicatesFromQueryParams(Root<Book> root, CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicates = new ArrayList<>();

        if (queryMap.containsKey("id")) {
            String id = queryMap.get("id");
            predicates.add(criteriaBuilder.equal(root.get("id"), Long.parseLong(id)));
        }

        if (queryMap.containsKey("title")) {
            String title = queryMap.get("title");
            predicates.add(criteriaBuilder
                    .like(criteriaBuilder.lower(root.get("title")), "%" + title.toLowerCase() + "%"));
        }

        if (queryMap.containsKey("subtitle")) {
            String subtitle = queryMap.get("subtitle");
            predicates.add(criteriaBuilder
                    .like(criteriaBuilder.lower(root.get("subtitle")), "%" + subtitle.toLowerCase() + "%"));
        }

        if (queryMap.containsKey("creationDate")) {
            String creationDate = queryMap.get("creationDate");
            predicates.add(criteriaBuilder.equal(root.get("creationDate"), LocalDate.parse(creationDate)));
        }

        if (queryMap.containsKey("isbn")) {
            String isbn = queryMap.get("isbn");
            predicates.add(criteriaBuilder.equal(root.get("isbn"), isbn));
        }

        if (queryMap.containsKey("firstName")) {
            String firstName = queryMap.get("firstName");
            Join<Book, Author> joinAuthors = root.join("authors", JoinType.INNER);
            predicates.add(criteriaBuilder.like(
                    criteriaBuilder.lower(joinAuthors.get("firstName")), "%" + firstName.toLowerCase() + "%"));
        }

        if (queryMap.containsKey("lastName")) {
            String lastName = queryMap.get("lastName");
            Join<Book, Author> joinAuthors = root.join("authors", JoinType.INNER);
            predicates.add(criteriaBuilder.like(
                    criteriaBuilder.lower(joinAuthors.get("lastName")), "%" + lastName.toLowerCase() + "%"));
        }

        if (queryMap.containsKey("middleName")) {
            String middleName = queryMap.get("middleName");
            Join<Book, Author> joinAuthors = root.join("authors", JoinType.INNER);
            predicates.add(criteriaBuilder.like(
                    criteriaBuilder.lower(joinAuthors.get("middleName")), "%" + middleName.toLowerCase() + "%"));
        }

        if (queryMap.containsKey("author_id")) {
            String id = queryMap.get("author_id");
            Join<Book, Author> joinAuthors = root.join("authors", JoinType.INNER);
            predicates.add(criteriaBuilder.equal(joinAuthors.get("id"), Long.parseLong(id)));
        }

        return predicates;
    }

}
