package com.gitlab.nikolapantelicftn.hybridp1.book.model;

import javax.persistence.CascadeType;
import com.gitlab.nikolapantelicftn.hybridp1.rent.model.RentedBook;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.PositiveOrZero;
import java.time.LocalDate;
import java.util.List;
import java.util.ArrayList;
import java.util.Objects;

@Entity
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotBlank
    private String title;
    private String subtitle;
    @NotNull
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinTable
    private List<Author> authors = new ArrayList<>();
    @OneToMany(mappedBy = "book", cascade = CascadeType.REMOVE)
    private List<RentedBook> rentedBooks = new ArrayList<>();
    private LocalDate creationDate;
    @NotNull
    @Column(unique = true)
    @Pattern(regexp = "(?=(?:\\D*\\d){10}(?:(?:\\D*\\d){3})?$)[\\d-]+$", message = "Invalid ISBN.")
    private String isbn;
    @PositiveOrZero
    private Integer quantity;
    private String imageUrl;

    public Book() {
    }

    public Book(Long id, String title, String subtitle, List<Author> authors, LocalDate creationDate, String isbn, Integer quantity, String imageUrl) {
        init(id, title, subtitle, authors, null, creationDate, isbn, quantity, imageUrl);
    }

    public Book(String title, String subtitle, List<Author> authors, LocalDate creationDate, String isbn, Integer quantity, String imageUrl) {
        init(null, title, subtitle, authors, null, creationDate, isbn, quantity, imageUrl);
    }

    private void init(Long id,
                      String title,
                      String subtitle,
                      List<Author> authors,
                      List<RentedBook> rentedBooks,
                      LocalDate creationDate,
                      String isbn,
                      Integer quantity,
                      String imageUrl) {
        this.id = id;
        this.title = title;
        this.subtitle = subtitle;
        this.authors = authors;
        this.rentedBooks = rentedBooks;
        this.creationDate = creationDate;
        this.isbn = isbn;
        this.quantity = quantity;
        this.imageUrl = imageUrl;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    public List<RentedBook> getRentedBooks() {
        return rentedBooks;
    }

    public void setRentedBooks(List<RentedBook> rentedBooks) {
        this.rentedBooks = rentedBooks;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public void addQuantity(Integer quantity) {
        setQuantity(this.quantity + quantity);
    }

    public void removeQuantity(Integer quantity) {
        setQuantity(this.quantity - quantity);
    }

    public void removeOne() {
        removeQuantity(1);
    }

    public void returnOne() {
        addQuantity(1);
    }

    public boolean hasAvailableCopies() {
        return quantity > 0;
    }

    public boolean hasRentedCopies() {
        if (rentedBooks == null) {
            return false;
        }
        return rentedBooks.stream().anyMatch(rent -> !rent.isReturned());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return id.equals(book.id) && title.equals(book.title) && Objects.equals(subtitle, book.subtitle) && creationDate.equals(book.creationDate) && isbn.equals(book.isbn);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, subtitle, authors, creationDate, isbn);
    }

}
