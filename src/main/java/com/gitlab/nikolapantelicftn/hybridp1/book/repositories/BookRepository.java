package com.gitlab.nikolapantelicftn.hybridp1.book.repositories;

import com.gitlab.nikolapantelicftn.hybridp1.book.model.Book;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface BookRepository extends JpaRepository<Book, Long>, JpaSpecificationExecutor<Book> {

    @Query("select b from Book b order by size(b.rentedBooks) desc")
    Page<Book> getMostRented(Pageable pageable);

}
