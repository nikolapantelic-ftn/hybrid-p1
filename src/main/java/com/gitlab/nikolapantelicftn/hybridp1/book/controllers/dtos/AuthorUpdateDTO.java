package com.gitlab.nikolapantelicftn.hybridp1.book.controllers.dtos;

import javax.validation.constraints.NotBlank;

public class AuthorUpdateDTO {

    @NotBlank
    private String firstName;
    @NotBlank
    private String lastName;
    private String middleName;

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

}
