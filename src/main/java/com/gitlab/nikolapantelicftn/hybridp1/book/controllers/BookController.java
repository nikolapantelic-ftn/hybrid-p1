package com.gitlab.nikolapantelicftn.hybridp1.book.controllers;

import com.gitlab.nikolapantelicftn.hybridp1.book.controllers.dtos.BookUpdateDTO;
import com.gitlab.nikolapantelicftn.hybridp1.book.controllers.specifications.BookSpecification;
import com.gitlab.nikolapantelicftn.hybridp1.book.model.Book;
import com.gitlab.nikolapantelicftn.hybridp1.book.services.BookService;
import com.gitlab.nikolapantelicftn.hybridp1.book.controllers.dtos.NewBookDTO;
import com.gitlab.nikolapantelicftn.hybridp1.book.controllers.dtos.BookViewDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.annotation.Secured;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RequestMapping("api/books")
@RestController
public class BookController {

    private static final Logger logger = LoggerFactory.getLogger(BookController.class);

    private final BookService service;
    private final ModelMapper mapper;

    public BookController(BookService service, ModelMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @Operation(summary = "Get all books, or filter them by query parameters")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Array of books", content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, array = @ArraySchema(
                            schema = @Schema(implementation = BookViewDTO.class)
                    ))
            })
    })
    @GetMapping
    public List<BookViewDTO> get(@RequestParam(required = false) Map<String, String> queryMap) {
        List<Book> foundBooks = service.get(
                new BookSpecification(queryMap),
                queryMap.getOrDefault("page", null));
        return foundBooks.stream()
                .map(book -> mapper.map(book, BookViewDTO.class))
                .collect(Collectors.toList());
    }

    @Secured("ROLE_ADMIN")
    @Operation(summary = "Create a new book")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Book successfully created", content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = BookViewDTO.class))
            }),
            @ApiResponse(responseCode = "400", description = "Invalid Book JSON format", content = @Content),
    })
    @PostMapping
    public BookViewDTO create(@Valid @RequestBody NewBookDTO newBook) {
        logger.info("POST '/api/books': creating book with title: {}", newBook.getTitle());
        Book created = service.create(mapper.map(newBook, Book.class));
        return mapper.map(created, BookViewDTO.class);
    }

    @Secured("ROLE_ADMIN")
    @Operation(summary = "Delete a book by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Book successfully deleted", content = @Content),
            @ApiResponse(responseCode = "404", description = "Book is not found", content = @Content)
    })
    @DeleteMapping("{id}")
    public void delete(@PathVariable Long id) {
        logger.info("DELETE '/api/books/{}': deleting book with id: {}", id, id);
        service.delete(id);
    }

    @Secured("ROLE_ADMIN")
    @Operation(summary = "Update an existing book")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Book successfully updated", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = BookViewDTO.class))
            }),
            @ApiResponse(responseCode = "400", description = "Invalid Book JSON format", content = @Content),
            @ApiResponse(responseCode = "404", description = "Book is not found", content = @Content)
    })
    @PutMapping("{id}")
    public BookViewDTO update(@PathVariable Long id, @Valid @RequestBody BookUpdateDTO book) {
        logger.info("PUT '/api/books/{}: updating book with id: {}", id, id);

        Book updated = service.update(id, mapper.map(book, Book.class));
        return mapper.map(updated, BookViewDTO.class);
    }

}
