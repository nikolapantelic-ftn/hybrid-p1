package com.gitlab.nikolapantelicftn.hybridp1.book.controllers.dtos;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.time.LocalDate;
import java.util.List;

public class BookUpdateDTO {

    @NotBlank
    private String title;
    private String subtitle;
    @NotNull
    private List<BookAuthorDTO> authors;
    @NotNull
    private LocalDate creationDate;
    @NotNull
    @Pattern(regexp = "(?=(?:\\D*\\d){10}(?:(?:\\D*\\d){3})?$)[\\d-]+$")
    private String isbn;
    private Integer quantity;
    private String imageUrl;

    public BookUpdateDTO() {}

    public BookUpdateDTO(String title, String subtitle, List<BookAuthorDTO> authors, LocalDate creationDate, String isbn, Integer quantity, String imageUrl) {
        this.title = title;
        this.subtitle = subtitle;
        this.authors = authors;
        this.creationDate = creationDate;
        this.isbn = isbn;
        this.quantity = quantity;
        this.imageUrl = imageUrl;
    }

    public String getTitle() {
        return title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public List<BookAuthorDTO> getAuthors() {
        return authors;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public String getIsbn() {
        return isbn;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public String getImageUrl() {
        return imageUrl;
    }

}
