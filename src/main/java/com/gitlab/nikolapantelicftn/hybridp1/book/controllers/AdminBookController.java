package com.gitlab.nikolapantelicftn.hybridp1.book.controllers;

import com.gitlab.nikolapantelicftn.hybridp1.book.controllers.dtos.BookViewDTO;
import com.gitlab.nikolapantelicftn.hybridp1.book.services.BookService;
import com.gitlab.nikolapantelicftn.hybridp1.rent.controllers.dtos.RentedBookViewDTO;
import com.gitlab.nikolapantelicftn.hybridp1.rent.services.RentalService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/admin/books")
@Secured("ROLE_ADMIN")
public class AdminBookController {

    private static final Logger logger = LoggerFactory.getLogger(AdminBookController.class);

    private final BookService bookService;
    private final RentalService rentalService;
    private final ModelMapper mapper;

    public AdminBookController(BookService bookService, RentalService rentalService, ModelMapper mapper) {
        this.bookService = bookService;
        this.rentalService = rentalService;
        this.mapper = mapper;
    }

    @Operation(summary = "Get a specified amount of most rented books.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Array of most rented books", content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = BookViewDTO.class))
            })
    })
    @GetMapping("{amount}")
    public List<BookViewDTO> getMostRented(@PathVariable Integer amount) {

        logger.info("GET '/api/admin/books': Admin is fetching most rented books.");

        return this.bookService.getMostRented(amount).stream()
                .map(book -> mapper.map(book, BookViewDTO.class))
                .collect(Collectors.toList());
    }

    @Operation(summary = "Get overdue rented books.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Array of overdue rented books", content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = BookViewDTO.class))
            })
    })
    @GetMapping
    public List<RentedBookViewDTO> getOverdue() {
        return rentalService.getAllOverdue().stream()
                .map(rent -> mapper.map(rent, RentedBookViewDTO.class))
                .collect(Collectors.toList());
    }

}
