package com.gitlab.nikolapantelicftn.hybridp1.book.repositories;

import com.gitlab.nikolapantelicftn.hybridp1.book.model.Author;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorRepository extends JpaRepository<Author, Long> {

}
