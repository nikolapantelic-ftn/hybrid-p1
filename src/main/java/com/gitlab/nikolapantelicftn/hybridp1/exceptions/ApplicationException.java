package com.gitlab.nikolapantelicftn.hybridp1.exceptions;

public class ApplicationException extends RuntimeException {

    public ApplicationException() {
        super();
    }

    public ApplicationException(String message) {
        super(message);
    }

}
