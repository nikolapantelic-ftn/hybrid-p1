package com.gitlab.nikolapantelicftn.hybridp1.exceptions;

import com.gitlab.nikolapantelicftn.hybridp1.rent.exceptions.InvalidRenterException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.stream.Collectors;

@RestControllerAdvice
public class GlobalExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);


    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(EntityNotFoundException.class)
    String handleEntityNotFound(EntityNotFoundException e) {
        logger.info("NOT_FOUND: {}", e.getMessage());
        logger.trace(e.getMessage(), e);

        return e.getMessage();
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    String handleMethodArgumentNotValid(MethodArgumentNotValidException e) {
        logger.info("BAD_REQUEST: {}", e.getMessage());
        logger.trace(e.getMessage(), e);

        return e.getFieldErrors().stream()
                .map(error -> error.getField() + " " + error.getDefaultMessage())
                .collect(Collectors.joining(", "));
    }

    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ExceptionHandler(InvalidRenterException.class)
    String handleInvalidRenter(InvalidRenterException e) {
        logger.info("FORBIDDEN: {}", e.getMessage());
        logger.trace(e.getMessage(), e);

        return e.getMessage();
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ApplicationException.class)
    String handleApplicationException(ApplicationException e) {
        logger.info("BAD_REQUEST: {}", e.getMessage());
        logger.trace(e.getMessage(), e);

        return e.getMessage();
    }

    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ExceptionHandler(BadCredentialsException.class)
    String handleBadCredentials(BadCredentialsException e) {
        logger.info("UNAUTHORIZED: {}", e.getMessage());
        logger.trace(e.getMessage());

        return e.getMessage();
    }

    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ExceptionHandler(AccessDeniedException.class)
    String handleAccessDenied(AccessDeniedException e) {
        logger.info("FORBIDDEN: {}", e.getMessage());
        logger.trace(e.getMessage());

        return e.getMessage();
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(RuntimeException.class)
    String handleExceptionDefault(RuntimeException e) {
        logger.error(e.getMessage(), e);
        return "Internal server error has occurred!";
    }

}
