package com.gitlab.nikolapantelicftn.hybridp1.auth.services;

import com.gitlab.nikolapantelicftn.hybridp1.auth.exceptions.JwtEncryptionException;
import com.gitlab.nikolapantelicftn.hybridp1.auth.jwt.UserJwtEncoder;
import com.gitlab.nikolapantelicftn.hybridp1.auth.model.AuthRequest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class AuthService {

    private final AuthenticationManager authenticationManager;
    private final UserDetailsService userDetailsService;
    private final UserJwtEncoder tokenEncoder;

    public AuthService(AuthenticationManager authenticationManager, UserDetailsService userDetailsService, UserJwtEncoder tokenEncoder) {
        this.authenticationManager = authenticationManager;
        this.userDetailsService = userDetailsService;
        this.tokenEncoder = tokenEncoder;
    }

    public String login(AuthRequest authRequest) {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authRequest.getUsername(), authRequest.getPassword()));
            UserDetails userDetails = userDetailsService.loadUserByUsername(authRequest.getUsername());
            return encryptToken(userDetails);
        } catch(AuthenticationException e) {
            throw new BadCredentialsException("Bad credentials!");
        }
    }

    private String encryptToken(UserDetails userDetails) {
        try {
            return tokenEncoder.encrypt(userDetails);
        } catch (IOException e) {
            throw new JwtEncryptionException();
        }
    }

}
