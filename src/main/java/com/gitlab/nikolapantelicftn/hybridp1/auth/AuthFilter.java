package com.gitlab.nikolapantelicftn.hybridp1.auth;

import com.gitlab.nikolapantelicftn.hybridp1.auth.exceptions.JwtDecryptionException;
import com.gitlab.nikolapantelicftn.hybridp1.auth.jwt.UserJwtEncoder;
import com.gitlab.nikolapantelicftn.hybridp1.auth.model.AuthToken;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class AuthFilter extends OncePerRequestFilter {

    private final UserJwtEncoder tokenEncoder;
    private final UserDetailsService userDetailsService;

    public AuthFilter(UserJwtEncoder tokenEncoder, UserDetailsService userDetailsService) {
        this.tokenEncoder = tokenEncoder;
        this.userDetailsService = userDetailsService;
    }

    @Override
    protected void doFilterInternal(
            HttpServletRequest request,
            HttpServletResponse response,
            FilterChain chain) throws ServletException, IOException {

        final String authHeader = request.getHeader(HttpHeaders.AUTHORIZATION);
        String jwtToken = getJwtTokenFromHeader(authHeader);

        if (jwtToken == null) {
            chain.doFilter(request, response);
            return;
        }

        AuthToken authToken = decryptToken(jwtToken);

        if (authToken.isExpired()) {
            chain.doFilter(request, response);
            return;
        }

        String username = authToken.getUsername();

        UserDetails authenticated = userDetailsService.loadUserByUsername(username);

        UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(
                authenticated,
                null,
                authenticated.getAuthorities()
        );

        SecurityContextHolder.getContext().setAuthentication(auth);
        chain.doFilter(request, response);
    }

    private AuthToken decryptToken(String jwtToken) {
        try {
            return tokenEncoder.decrypt(jwtToken);
        } catch (IOException e) {
            throw new JwtDecryptionException();
        }
    }

    private String getJwtTokenFromHeader(String authHeader) {
        if (authHeader == null || authHeader.isEmpty() || !authHeader.startsWith("Bearer ")) {
            return null;
        }

        String[] headerTokens = authHeader.split(" ");
        return headerTokens.length == 2 ? headerTokens[1] : null;
    }

}
