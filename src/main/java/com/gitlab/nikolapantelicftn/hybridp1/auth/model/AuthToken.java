package com.gitlab.nikolapantelicftn.hybridp1.auth.model;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class AuthToken {

    public static final Duration VALIDITY_DURATION = Duration.ofDays(1);

    private String username;
    private LocalDateTime expires;

    public AuthToken() {}

    public AuthToken(String username) {
        this.username = username;
    }

    public AuthToken(String username, LocalDateTime expires) {
        this.username = username;
        this.expires = expires;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public LocalDateTime getExpires() {
        return expires;
    }

    public void setExpires(LocalDateTime expires) {
        this.expires = expires;
    }

    public boolean isExpired() {
        return LocalDateTime.now().isAfter(expires);
    }

}
