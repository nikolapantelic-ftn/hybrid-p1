package com.gitlab.nikolapantelicftn.hybridp1.auth.exceptions;

public class JwtDecryptionException extends JwtException {

    public JwtDecryptionException() {
        super("An error has occurred while decrypting JWT!");
    }

    public JwtDecryptionException(String message) {
        super(message);
    }

}
