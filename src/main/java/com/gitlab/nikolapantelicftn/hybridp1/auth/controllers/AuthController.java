package com.gitlab.nikolapantelicftn.hybridp1.auth.controllers;

import com.gitlab.nikolapantelicftn.hybridp1.auth.model.AuthRequest;
import com.gitlab.nikolapantelicftn.hybridp1.auth.services.AuthService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("api/auth")
public class AuthController {

    private final AuthService authService;

    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    @PostMapping
    public ResponseEntity<Void> login(@RequestBody AuthRequest authRequest) {
        String generatedToken = authService.login(authRequest);
        return ResponseEntity.ok().header(HttpHeaders.AUTHORIZATION, generatedToken).build();
    }

}
