package com.gitlab.nikolapantelicftn.hybridp1.auth.jwt;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.nikolapantelicftn.hybridp1.auth.model.AuthToken;
import com.gitlab.nikolapantelicftn.hybridp1.config.PropertyConfiguration;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
public class UserJwtEncoder {

    private static final String USER_CLAIM = "user";

    private final Algorithm algorithm;
    private final ObjectMapper objectMapper;
    private final JWTVerifier verifier;

    public UserJwtEncoder(PropertyConfiguration config, ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
        this.algorithm = Algorithm.HMAC256(config.getJwtSecret());
        this.verifier = JWT.require(algorithm).build();
    }

    public String encrypt(UserDetails userDetails) throws IOException {
        new Date();
        return JWT.create()
                .withClaim("username", userDetails.getUsername())
                .withClaim("role", userDetails.getAuthorities().toArray()[0].toString())
                .withClaim("expires", LocalDateTime.now().plus(AuthToken.VALIDITY_DURATION).toString())
                .sign(algorithm);
    }

    public AuthToken decrypt(String encryptedString) throws IOException {
        verifier.verify(encryptedString);
        DecodedJWT decoded = JWT.decode(encryptedString);
        String username = decoded.getClaim("username").asString();
        LocalDateTime expires = LocalDateTime.parse(decoded.getClaim("expires").asString());
        return new AuthToken(username, expires);
    }

}
