package com.gitlab.nikolapantelicftn.hybridp1.auth.exceptions;

public class JwtEncryptionException extends JwtException {

    public JwtEncryptionException() {
        super("An error has occurred while encrypting JWT!");
    }

    public JwtEncryptionException(String message) {
        super(message);
    }

}
