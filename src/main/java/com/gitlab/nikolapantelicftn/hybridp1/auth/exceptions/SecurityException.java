package com.gitlab.nikolapantelicftn.hybridp1.auth.exceptions;

import com.gitlab.nikolapantelicftn.hybridp1.exceptions.ApplicationException;

public class SecurityException extends ApplicationException {

    public SecurityException() {
        super("Authentication exception has occurred!");
    }

    public SecurityException(String message) {
        super(message);
    }

}
