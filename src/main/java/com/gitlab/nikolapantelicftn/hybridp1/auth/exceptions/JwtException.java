package com.gitlab.nikolapantelicftn.hybridp1.auth.exceptions;

public class JwtException extends SecurityException {

    public JwtException() {
        super("An error has occurred during JWT operation!");
    }

    public JwtException(String message) {
        super(message);
    }

}
