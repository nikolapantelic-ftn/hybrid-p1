package com.gitlab.nikolapantelicftn.hybridp1.media.controllers;

import com.gitlab.nikolapantelicftn.hybridp1.media.services.ImageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("api/images")
public class ImageController {

    private static final Logger logger = LoggerFactory.getLogger(ImageController.class);

    private final ImageService service;

    public ImageController(ImageService service) {
        this.service = service;
    }

    @Secured("ROLE_ADMIN")
    @PostMapping(produces = "text/plain")
    public String handleImageUpload(@RequestParam("image") MultipartFile file) {

        logger.info("POST '/api/images': Uploading new image.");

        return service.store(file);
    }

}
