package com.gitlab.nikolapantelicftn.hybridp1.media.services;

import com.gitlab.nikolapantelicftn.hybridp1.config.PropertyConfiguration;
import com.gitlab.nikolapantelicftn.hybridp1.exceptions.ApplicationException;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;

import static java.nio.file.StandardOpenOption.CREATE_NEW;

@Service
public class ImageService {

    private final String imagePath;

    public ImageService(PropertyConfiguration config) {
        this.imagePath = config.getImagePath();
    }

    public String store(MultipartFile file) {
        Objects.requireNonNull(file);

        createDirectoryIfNotExists();

        String extension = getExtension(file);

        String imageName = System.currentTimeMillis() + "." + extension;
        try (OutputStream os = Files.newOutputStream(Paths.get(imagePath, imageName), CREATE_NEW)) {
            os.write(file.getBytes());
        } catch (IOException e) {
            throw new ApplicationException("Error uploading image.");
        }
        return imageName;
    }

    private String getExtension(MultipartFile file) {
        String fileName = file.getOriginalFilename();
        String extension = null;
        if (fileName != null) {
            extension = fileName
                    .substring(fileName.lastIndexOf(".") + 1)
                    .toLowerCase();
        }
        return extension;
    }

    private void createDirectoryIfNotExists() {
        if (!Files.exists(Paths.get(imagePath))) {
            try {
                Files.createDirectories(Paths.get(imagePath));
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

}
