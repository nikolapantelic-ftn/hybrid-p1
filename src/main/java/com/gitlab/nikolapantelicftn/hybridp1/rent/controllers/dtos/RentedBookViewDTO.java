package com.gitlab.nikolapantelicftn.hybridp1.rent.controllers.dtos;

import com.gitlab.nikolapantelicftn.hybridp1.book.controllers.dtos.BookViewDTO;
import com.gitlab.nikolapantelicftn.hybridp1.user.controllers.dtos.UserViewDTO;
import org.apache.tomcat.jni.Local;

import java.time.Duration;
import java.time.LocalDate;
import java.time.Period;

public class RentedBookViewDTO {

    private Long id;
    private BookViewDTO book;
    private UserViewDTO renter;
    private LocalDate rentingDate;
    private Period rentingPeriod;
    private boolean returned;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BookViewDTO getBook() {
        return book;
    }

    public void setBook(BookViewDTO book) {
        this.book = book;
    }

    public UserViewDTO getRenter() {
        return renter;
    }

    public void setRenter(UserViewDTO renter) {
        this.renter = renter;
    }

    public LocalDate getRentingDate() {
        return rentingDate;
    }

    public void setRentingDate(LocalDate rentingDate) {
        this.rentingDate = rentingDate;
    }

    public Period getRentingPeriod() {
        return rentingPeriod;
    }

    public void setRentingPeriod(Period rentingPeriod) {
        this.rentingPeriod = rentingPeriod;
    }

    public boolean isReturned() {
        return returned;
    }

    public void setReturned(boolean returned) {
        this.returned = returned;
    }

    public LocalDate getReturnDate() {
        return rentingDate.plus(rentingPeriod);
    }

    public Integer getDaysRemaining() {
        return Period.between(LocalDate.now(), getReturnDate()).getDays();
    }

}
