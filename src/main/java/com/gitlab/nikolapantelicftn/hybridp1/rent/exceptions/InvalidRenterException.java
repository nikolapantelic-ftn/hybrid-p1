package com.gitlab.nikolapantelicftn.hybridp1.rent.exceptions;

import com.gitlab.nikolapantelicftn.hybridp1.exceptions.ApplicationException;

public class InvalidRenterException extends ApplicationException {

    public InvalidRenterException() {
        super("Rented username doesn't match Rented Book's username!");
    }

}
