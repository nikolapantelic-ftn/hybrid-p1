package com.gitlab.nikolapantelicftn.hybridp1.rent.repositories;

import com.gitlab.nikolapantelicftn.hybridp1.rent.model.RentedBook;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RentalRepository extends JpaRepository<RentedBook, Long> {
    List<RentedBook> findAllByReturnedFalse();
    List<RentedBook> findAllByRenterUsernameAndReturnedFalse(String username);
}
