package com.gitlab.nikolapantelicftn.hybridp1.rent.controllers;

import com.gitlab.nikolapantelicftn.hybridp1.rent.controllers.dtos.RentRequestDTO;
import com.gitlab.nikolapantelicftn.hybridp1.rent.controllers.dtos.RentedBookViewDTO;
import com.gitlab.nikolapantelicftn.hybridp1.rent.model.RentedBook;
import com.gitlab.nikolapantelicftn.hybridp1.rent.services.RentalService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/rent")
public class RentalController {

    private static final Logger logger = LoggerFactory.getLogger(RentalController.class);

    private final RentalService service;
    private final ModelMapper mapper;

    public RentalController(RentalService service, ModelMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @Operation(summary = "Rent a book to a given user")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Book successfully rented", content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, schema = @Schema(implementation = RentedBookViewDTO.class))
            }),
            @ApiResponse(responseCode = "404", description = "Book or user not found", content = @Content)
    })
    @PostMapping
    public RentedBookViewDTO rentBook(@RequestBody RentRequestDTO rentRequest) {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        logger.info("POST '/api/rent': Renting book with id: {}, for user: {}",
                rentRequest.getBookId(),
                currentPrincipalName);

        RentedBook created = service.rentBook(rentRequest.getBookId(), currentPrincipalName);
        return mapper.map(created, RentedBookViewDTO.class);
    }

    @Secured("ROLE_ADMIN")
    @Operation(summary = "Return a book")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Book successfully returned"),
            @ApiResponse(responseCode = "400", description = "Book has rented copies"),
            @ApiResponse(responseCode = "403", description = "User didn't rent specified book")
    })
    @PutMapping("{id}")
    public void returnBook(@PathVariable Long id) {

        logger.info("GET 'api/rent/{}: Returning rented book with rent id: {}", id, id);

        service.returnBook(id);
    }

    @Secured("ROLE_ADMIN")
    @GetMapping("user/{username}")
    public List<RentedBookViewDTO> get(@PathVariable String username) {
        return service.get(username).stream()
                .map(rentedBook -> mapper.map(rentedBook, RentedBookViewDTO.class))
                .collect(Collectors.toList());
    }

    @Secured("ROLE_ADMIN")
    @GetMapping("{id}")
    public RentedBookViewDTO get(@PathVariable Long id) {
        return mapper.map(service.get(id), RentedBookViewDTO.class);
    }

    @Secured("ROLE_ADMIN")
    @GetMapping
    public List<RentedBookViewDTO> get() {
        return service.getAll().stream()
                .map(rentedBook -> mapper.map(rentedBook, RentedBookViewDTO.class))
                .collect(Collectors.toList());
    }

    @GetMapping("personal")
    public List<RentedBookViewDTO> getForUser(Principal user) {
        return service.get(user.getName()).stream()
                .map(rentedBook -> mapper.map(rentedBook, RentedBookViewDTO.class))
                .collect(Collectors.toList());
    }

}
