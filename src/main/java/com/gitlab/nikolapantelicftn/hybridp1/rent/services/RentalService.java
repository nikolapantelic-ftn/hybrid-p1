package com.gitlab.nikolapantelicftn.hybridp1.rent.services;

import com.gitlab.nikolapantelicftn.hybridp1.book.model.Book;
import com.gitlab.nikolapantelicftn.hybridp1.book.services.BookService;
import com.gitlab.nikolapantelicftn.hybridp1.exceptions.ApplicationException;
import com.gitlab.nikolapantelicftn.hybridp1.rent.exceptions.RentedBookNotFoundException;
import com.gitlab.nikolapantelicftn.hybridp1.rent.repositories.RentalRepository;
import com.gitlab.nikolapantelicftn.hybridp1.rent.model.RentedBook;
import com.gitlab.nikolapantelicftn.hybridp1.user.model.User;
import com.gitlab.nikolapantelicftn.hybridp1.user.services.UserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Period;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class RentalService {

    public static final Period RENTING_PERIOD = Period.ofDays(14);

    private final RentalRepository repository;
    private final BookService bookService;
    private final UserService userService;

    public RentalService(RentalRepository repository,
                         BookService bookService,
                         UserService userService) {
        this.repository = repository;
        this.bookService = bookService;
        this.userService = userService;
    }

    @Transactional
    public RentedBook rentBook(Long bookId, String renterUsername) {
        Book forRent = bookService.get(bookId);
        User renter = userService.get(renterUsername);
        return rentBook(forRent, renter);
    }

    @Transactional
    public RentedBook rentBook(Book book, User renter) {
        Objects.requireNonNull(book);
        Objects.requireNonNull(renter);
        decrementBookQuantity(book);
        return createRentEntry(book, renter);
    }

    @Transactional
    public void returnBook(Long rentalId) {
        RentedBook rent = repository.findById(rentalId)
                .orElseThrow(() -> new RentedBookNotFoundException(rentalId));
        rent.returnBook();
        Book forUpdate = rent.getBook();
        incrementBookQuantity(forUpdate);
        repository.save(rent);
    }

    private void incrementBookQuantity(Book book) {
        book.returnOne();
        bookService.update(book.getId(), book);
    }

    private void decrementBookQuantity(Book book) {
        if (!book.hasAvailableCopies()) {
            throw new ApplicationException(
                    String.format("Book '%s' has no available copies!", book.getTitle()));
        }
        book.removeOne();
        bookService.update(book.getId(), book);
    }

    private RentedBook createRentEntry(Book book, User renter) {
        RentedBook newRent = new RentedBook(book, renter, RENTING_PERIOD);
        return repository.save(newRent);
    }

    @Transactional
    public List<RentedBook> getAllOverdue() {
        return repository.findAllByReturnedFalse().stream()
                .filter(RentedBook::isOverdue)
                .collect(Collectors.toList());
    }

    @Transactional
    public List<RentedBook> get(String username) {
        return repository.findAllByRenterUsernameAndReturnedFalse(username);
    }

    public RentedBook get(Long id) {
        return repository.findById(id)
                .orElseThrow(() -> new RentedBookNotFoundException(id));
    }

    @Transactional
    public List<RentedBook> getAll() {
        return repository.findAllByReturnedFalse();
    }

}
