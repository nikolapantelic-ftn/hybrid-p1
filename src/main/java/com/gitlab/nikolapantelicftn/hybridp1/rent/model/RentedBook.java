package com.gitlab.nikolapantelicftn.hybridp1.rent.model;

import com.gitlab.nikolapantelicftn.hybridp1.book.model.Book;
import com.gitlab.nikolapantelicftn.hybridp1.user.model.User;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.Period;

@Entity
public class RentedBook {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotNull
    @ManyToOne
    private Book book;
    @NotNull
    @ManyToOne
    private User renter;
    @NotNull
    private LocalDate rentingDate;
    @NotNull
    private Period rentingPeriod;
    private boolean returned;

    public RentedBook(Long id, Book book, User renter, LocalDate rentingDate, Period rentingPeriod, boolean returned) {
        init(id, book, renter, rentingDate, rentingPeriod, returned);
    }

    public RentedBook(Book book, User renter, Period rentingPeriod) {
        init(null, book, renter, LocalDate.now(), rentingPeriod, false);
    }

    private void init(Long id, Book book, User renter, LocalDate rentingDate, Period rentingPeriod, boolean returned) {
        this.id = id;
        this.book = book;
        this.renter = renter;
        this.rentingDate = rentingDate;
        this.rentingPeriod = rentingPeriod;
        this.returned = returned;
    }

    public RentedBook() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public User getRenter() {
        return renter;
    }

    public void setRenter(User renter) {
        this.renter = renter;
    }

    public LocalDate getRentingDate() {
        return rentingDate;
    }

    public void setRentingDate(LocalDate rentingDate) {
        this.rentingDate = rentingDate;
    }

    public Period getRentingPeriod() {
        return rentingPeriod;
    }

    public void setRentingPeriod(Period rentingPeriod) {
        this.rentingPeriod = rentingPeriod;
    }

    public boolean isReturned() {
        return returned;
    }

    public void setReturned(boolean returned) {
        this.returned = returned;
    }

    public boolean isOverdue() {
        return LocalDate.now().isAfter(rentingDate.plus(rentingPeriod));
    }

    public void returnBook() {
        this.returned = true;
    }

}
