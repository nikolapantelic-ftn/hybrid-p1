package com.gitlab.nikolapantelicftn.hybridp1.rent.exceptions;

import com.gitlab.nikolapantelicftn.hybridp1.exceptions.EntityNotFoundException;
import com.gitlab.nikolapantelicftn.hybridp1.rent.model.RentedBook;

public class RentedBookNotFoundException extends EntityNotFoundException {

    public RentedBookNotFoundException(Long id) {
        super(id, RentedBook.class);
    }

}
