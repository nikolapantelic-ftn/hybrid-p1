package com.gitlab.nikolapantelicftn.hybridp1.util;

import com.gitlab.nikolapantelicftn.hybridp1.auth.model.AuthRequest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

public class TestSecurity {

    public static String getAuthorizationHeader(MockMvc mvc, AuthRequest authRequest) throws Exception {
        String userJson = "{\"username\":\"" +
                authRequest.getUsername() +
                "\", \"password\":\"" +
                authRequest.getPassword() +
                "\"}";

        MvcResult userResponse = mvc.perform(post("/api/auth")
                .contentType(MediaType.APPLICATION_JSON)
                .content(userJson))
                .andReturn();
        return "Bearer " + userResponse.getResponse().getHeader(HttpHeaders.AUTHORIZATION);
    }

}
