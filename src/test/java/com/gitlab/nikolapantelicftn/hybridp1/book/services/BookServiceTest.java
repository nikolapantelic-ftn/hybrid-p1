package com.gitlab.nikolapantelicftn.hybridp1.book.services;

import com.gitlab.nikolapantelicftn.hybridp1.book.exceptions.BookNotFoundException;
import com.gitlab.nikolapantelicftn.hybridp1.book.model.Author;
import com.gitlab.nikolapantelicftn.hybridp1.book.model.Book;
import com.gitlab.nikolapantelicftn.hybridp1.book.repositories.BookRepository;
import com.gitlab.nikolapantelicftn.hybridp1.exceptions.ApplicationException;
import com.gitlab.nikolapantelicftn.hybridp1.rent.model.RentedBook;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
public class BookServiceTest {

    @Mock
    BookRepository repository;

    @Mock AuthorService authorService;

    @InjectMocks
    BookService service;

    private static Book book;
    private static Book newBook;
    private static List<Book> books;
    private static Author author;

    @BeforeAll
    static void arrange() {
        author = new Author("Name", "Lastname");
        List<Author> authors = List.of(author);
        book = new Book(1L, "title", "subtitle", authors, LocalDate.now(), "isbn", 5, "imageUrl");
        newBook = new Book("title", "subtitle", authors, LocalDate.now(), "isbn", 5, "imageUrl");
        books = List.of(book);
    }

    @Test
    public void create_Success() {
        Mockito.when(repository.save(newBook)).thenReturn(book);
        Mockito.when(authorService.exists(null)).thenReturn(false);
        Mockito.when(authorService.create(author)).thenReturn(author);

        Book result = service.create(newBook);

        Mockito.verify(repository, Mockito.times(1)).save(newBook);

        Assertions.assertEquals(book, result);
    }

    @Test
    public void create_Fail() {
        Assertions.assertThrows(NullPointerException.class, () -> {
            service.create(null);
        });
    }

    @Test
    public void getAll_Success() {
        Mockito.when(repository.findAll()).thenReturn(books);

        List<Book> result = service.getAll();

        Mockito.verify(repository, Mockito.times(1)).findAll();

        Assertions.assertEquals(books, result);
    }

    @Test
    public void getById_Success() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(book));

        Book result = service.get(1L);

        Mockito.verify(repository, Mockito.times(1)).findById(1L);

        Assertions.assertEquals(book, result);
    }

    @Test
    public void getById_Fail() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.empty());

        Assertions.assertThrows(BookNotFoundException.class, () -> {
            service.get(1L);
        });
    }

    @Test
    public void delete_Success() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(book));

        service.delete(1L);

        Mockito.verify(repository, Mockito.times(1)).deleteById(1L);
    }

    @Test
    public void delete_Fail() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.empty());

        Assertions.assertThrows(BookNotFoundException.class, () -> {
            service.delete(1L);
        });
    }

    @Test
    public void delete_FailHasRentedCopies() {
        Book withRentedCopies = new Book(1L, "title", "subtitle", List.of(author), LocalDate.now(), "isbn", 5, "imageUrl");
        withRentedCopies.setRentedBooks(List.of(new RentedBook()));
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(withRentedCopies));

        Assertions.assertThrows(ApplicationException.class, () -> {
            service.delete(1L);
        });
    }

    @Test
    public void update_FailBookNull() {
        Assertions.assertThrows(NullPointerException.class, () -> {
            service.update(1L, null);
        });
    }

    @Test
    public void update_FailBookNotExists() {
        Mockito.when(repository.existsById(1L)).thenReturn(false);

        Assertions.assertThrows(BookNotFoundException.class, () -> {
            service.update(1L, newBook);
        });
    }

    @Test
    public void update_Success() {
        Mockito.when(repository.existsById(1L)).thenReturn(true);
        Mockito.when(repository.save(newBook)).thenReturn(book);

        Book result = service.update(1L, newBook);

        Mockito.verify(repository, Mockito.times(1)).save(any());

        Assertions.assertEquals(book, result);
    }

}
