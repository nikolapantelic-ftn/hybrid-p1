package com.gitlab.nikolapantelicftn.hybridp1.book.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.nikolapantelicftn.hybridp1.auth.model.AuthRequest;
import com.gitlab.nikolapantelicftn.hybridp1.book.controllers.dtos.AuthorViewDTO;
import com.gitlab.nikolapantelicftn.hybridp1.book.controllers.dtos.BookAuthorDTO;
import com.gitlab.nikolapantelicftn.hybridp1.book.controllers.dtos.BookUpdateDTO;
import com.gitlab.nikolapantelicftn.hybridp1.book.controllers.dtos.NewBookDTO;
import com.gitlab.nikolapantelicftn.hybridp1.book.model.Book;
import com.gitlab.nikolapantelicftn.hybridp1.book.repositories.BookRepository;
import com.gitlab.nikolapantelicftn.hybridp1.util.TestSecurity;
import org.hamcrest.core.StringContains;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class BookIntegrationTest {

    @Autowired
    MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private BookRepository repository;

    private String userHeader;
    private String adminHeader;

    @BeforeEach
    void login() throws Exception {
        AuthRequest userLogin = new AuthRequest("user1", "pass");
        userHeader = TestSecurity.getAuthorizationHeader(mvc, userLogin);

        AuthRequest adminLogin = new AuthRequest("admin1", "pass");
        adminHeader = TestSecurity.getAuthorizationHeader(mvc, adminLogin);
    }

    @Test
    public void get_Success() throws Exception {
        final Long id = 1L;
        List<Book> expected = List.of(repository.findById(id).orElseThrow());

        MvcResult result = mvc.perform(get("/api/books?id=" + id)
                .contentType(MediaType.APPLICATION_JSON).header(HttpHeaders.AUTHORIZATION, adminHeader))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn();

        String resultJson = result.getResponse().getContentAsString();
        List<Book> bookResult = List.of(objectMapper.readValue(resultJson, Book[].class));

        Assertions.assertEquals(expected, bookResult);
        assertThat(expected).hasSameElementsAs(bookResult);
    }

    @Test
    public void getAll_Success() throws Exception {
        List<Book> expected = repository.findAll();
        MvcResult result = mvc.perform(get("/api/books")
                .contentType(MediaType.APPLICATION_JSON).header(HttpHeaders.AUTHORIZATION, adminHeader))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andReturn();

        String resultJson = result.getResponse().getContentAsString();
        List<Book> booksResult = List.of(objectMapper.readValue(resultJson, Book[].class));

        assertThat(booksResult).hasSameElementsAs(expected);
    }

    @Test
    public void create_Success() throws Exception {
        NewBookDTO newBook = new NewBookDTO("title",
                "subtitle",
                List.of(new BookAuthorDTO("firstName", "middleName", "lastName")),
                LocalDate.now(),
                "978-3-16-123410-0", 5, "");
        String bookJson = objectMapper.writeValueAsString(newBook);
        Book expected = modelMapper.map(newBook, Book.class);

        MvcResult result = mvc.perform(post("/api/books")
                .contentType(MediaType.APPLICATION_JSON)
                .content(bookJson).header(HttpHeaders.AUTHORIZATION, adminHeader))
                .andExpect(status().isOk())
                .andReturn();

        String resultJson = result.getResponse().getContentAsString();
        Book bookResult = objectMapper.readValue(resultJson, Book.class);

        expected.setId(bookResult.getId());

        Assertions.assertEquals(expected, bookResult);
    }

    @Test
    public void create_ValidationError() throws Exception {
        NewBookDTO newBook = new NewBookDTO("",
                "",
                List.of(new BookAuthorDTO(1L, "firstName", "middleName", "lastName")),
                null,
                "notisbnformat", 5, "");
        String bookJson = objectMapper.writeValueAsString(newBook);

        mvc.perform(post("/api/books")
                .contentType(MediaType.APPLICATION_JSON)
                .content(bookJson).header(HttpHeaders.AUTHORIZATION, adminHeader))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(StringContains.containsString("creationDate must not be null")))
                .andExpect(content().string(StringContains.containsString("isbn must match")))
                .andExpect(content().string(StringContains.containsString("creationDate must not be null")));
    }

    @Test
    public void update_Success() throws Exception {
        final String isbn = "978-3-16-123410-1";
        final Long id = 1L;
        BookUpdateDTO bookUpdate = new BookUpdateDTO("title",
                "subtitle",
                List.of(new BookAuthorDTO("firstName", "middleName", "lastName")),
                LocalDate.now(),
                isbn,
                5, "");
        String bookJson = objectMapper.writeValueAsString(bookUpdate);
        Book expected = modelMapper.map(bookUpdate, Book.class);
        expected.setId(id);

        MvcResult result = mvc.perform(put("/api/books/" + id)
                .contentType(MediaType.APPLICATION_JSON)
                .content(bookJson).header(HttpHeaders.AUTHORIZATION, adminHeader))
                .andExpect(status().isOk())
                .andReturn();

        String resultJson = result.getResponse().getContentAsString();
        Book bookResult = objectMapper.readValue(resultJson, Book.class);

        Assertions.assertEquals(expected, bookResult);
    }

    @Test
    public void update_NotFound() throws Exception {
        final Long id = 1000L;
        BookUpdateDTO bookUpdate = new BookUpdateDTO("title",
                "subtitle",
                List.of(new BookAuthorDTO(id, "firstName", "middleName", "lastName")),
                LocalDate.now(),
                "978-3-16-123410-1",
                5, "");
        String bookJson = objectMapper.writeValueAsString(bookUpdate);

        mvc.perform(put("/api/books/" + id)
                .contentType(MediaType.APPLICATION_JSON)
                .content(bookJson).header(HttpHeaders.AUTHORIZATION, adminHeader))
                .andExpect(status().isNotFound());
    }

    @Test
    public void update_ValidationError() throws Exception {
        final Long id = 1L;
        BookUpdateDTO newBook = new BookUpdateDTO("",
                "",
                List.of(new BookAuthorDTO(id, "firstName", "middleName", "lastName")),
                null,
                "notisbnformat",
                5, "");
        String bookJson = objectMapper.writeValueAsString(newBook);

        mvc.perform(put("/api/books/" + id)
                .contentType(MediaType.APPLICATION_JSON)
                .content(bookJson).header(HttpHeaders.AUTHORIZATION, adminHeader))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(StringContains.containsString("creationDate must not be null")))
                .andExpect(content().string(StringContains.containsString("isbn must match")))
                .andExpect(content().string(StringContains.containsString("creationDate must not be null")));
    }

    @Test
    public void delete_Success() throws Exception {
        final Long id = 2L;
        mvc.perform(delete("/api/books/" + id).header(HttpHeaders.AUTHORIZATION, adminHeader))
                .andExpect(status().isOk());
    }

    @Test
    public void delete_NotFound() throws Exception {
        final Long id = 1000L;
        mvc.perform(delete("/api/books/" + id).header(HttpHeaders.AUTHORIZATION, adminHeader))
                .andExpect(status().isNotFound());
    }

}
