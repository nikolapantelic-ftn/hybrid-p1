package com.gitlab.nikolapantelicftn.hybridp1.user.services;

import com.gitlab.nikolapantelicftn.hybridp1.user.exceptions.UserExistsException;
import com.gitlab.nikolapantelicftn.hybridp1.user.exceptions.UserNotFoundException;
import com.gitlab.nikolapantelicftn.hybridp1.user.model.Role;
import com.gitlab.nikolapantelicftn.hybridp1.user.model.User;
import com.gitlab.nikolapantelicftn.hybridp1.user.repositories.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

    @Mock
    private UserRepository repository;

    @InjectMocks
    private UserService service;

    private static User user;
    private static User newUser;
    private static List<User> users;

    @BeforeAll
    static void arrange() {
        user = new User(1L, "johndoe", "John", "Doe", new Role("ROLE_USER"));
        newUser = new User("johndoe", "John", "Doe", new Role("ROLE_USER"));
        users = List.of(user);
    }

    @Test
    public void getAll_Success() {
        Mockito.when(repository.findAll()).thenReturn(users);

        List<User> result = service.getAll();

        Mockito.verify(repository, Mockito.times(1)).findAll();

        Assertions.assertEquals(users, result);
    }

    @Test
    public void getByUsername_Success() {
        Mockito.when(repository.findByUsername("johndoe")).thenReturn(Optional.of(user));

        User result = service.get("johndoe");

        Mockito.verify(repository, Mockito.times(1)).findByUsername("johndoe");

        Assertions.assertEquals(user, result);
    }

    @Test
    public void getByUsername_Fail() {
        Mockito.when(repository.findByUsername("johndoe")).thenReturn(Optional.empty());

        Assertions.assertThrows(UserNotFoundException.class, () -> {
            service.get("johndoe");
        });
    }

    @Test
    public void getById_Success() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(user));

        User result = service.get(1L);

        Mockito.verify(repository, Mockito.times(1)).findById(1L);

        Assertions.assertEquals(user, result);
    }

    @Test
    public void getById_Fail() {
        Mockito.when(repository.findById(1L)).thenReturn(Optional.empty());

        Assertions.assertThrows(UserNotFoundException.class, () -> {
            service.get(1L);
        });
    }

    @Test
    public void create_Success() {
        Mockito.when(repository.save(newUser)).thenReturn(user);

        User result = service.create(newUser);

        Mockito.verify(repository, Mockito.times(1)).save(newUser);

        Assertions.assertEquals(user, result);
    }

    @Test
    public void create_FailUserExists() {
        String existingUsername = "johndoe";
        Mockito.when(repository.existsByUsername(existingUsername)).thenReturn(true);

        Assertions.assertThrows(UserExistsException.class, () -> {
            service.create(newUser);
        });
    }

    @Test
    void create_FailNullArgument() {
        Assertions.assertThrows(NullPointerException.class, () -> {
            service.create(null);
        });
    }

    @Test
    void update_Success() {
        Mockito.when(repository.existsByUsername(newUser.getUsername())).thenReturn(false);
        Mockito.when(repository.save(newUser)).thenReturn(user);
        Mockito.when(repository.findById(user.getId())).thenReturn(Optional.of(user));

        User result = service.update(user.getId(), newUser);

        Mockito.verify(repository, Mockito.times(1)).save(newUser);

        Assertions.assertEquals(user, result);
    }

    @Test
    void update_FailExistingUsername() {
        User other = new User(1L, "johnsmith", "John", "Doe", new Role("ROLE_USER"));
        Mockito.when(repository.existsByUsername(newUser.getUsername())).thenReturn(true);
        Mockito.when(repository.findById(user.getId())).thenReturn(Optional.of(other));

        Assertions.assertThrows(UserExistsException.class, () -> {
            service.update(user.getId(), newUser);
        });
    }

    @Test
    void update_FailUserNotFound() {
        Mockito.when(repository.findById(user.getId())).thenReturn(Optional.empty());
        Mockito.when(repository.existsByUsername(newUser.getUsername())).thenReturn(false);

        Assertions.assertThrows(UserNotFoundException.class, () -> {
            service.update(user.getId(), newUser);
        });
    }

    @Test
    void update_FailNullArgument() {
        Assertions.assertThrows(NullPointerException.class, () -> {
            service.update(user.getId(), null);
        });
    }

    @Test
    void deleteById_Success() {
        Mockito.when(repository.existsById(user.getId())).thenReturn(true);

        service.delete(user.getId());

        Mockito.verify(repository, Mockito.times(1)).deleteById(user.getId());
    }

    @Test
    void deleteById_Fail() {
        Mockito.when(repository.existsById(user.getId())).thenReturn(false);

        Assertions.assertThrows(UserNotFoundException.class, () -> {
            service.delete(user.getId());
        });
    }

    @Test
    void deleteByUsername_Success() {
        Mockito.when(repository.existsByUsername(user.getUsername())).thenReturn(true);

        service.delete(user.getUsername());

        Mockito.verify(repository, Mockito.times(1)).deleteByUsername(user.getUsername());
    }

    @Test
    void deleteByUsername_Fail() {
        Mockito.when(repository.existsByUsername(user.getUsername())).thenReturn(false);

        Assertions.assertThrows(UserNotFoundException.class, () -> {
            service.delete(user.getUsername());
        });
    }

    @Test
    void updateUsername_Success() {
        String oldUsername = user.getUsername();
        String newUsername = "johnsmith";
        User updated = new User(1L, newUsername, "John", "Doe", new Role("ROLE_USER"));
        Mockito.when(repository.existsByUsername(newUsername)).thenReturn(false);
        Mockito.when(repository.findByUsername(oldUsername)).thenReturn(Optional.of(user));

        service.updateUsername(oldUsername, newUsername);

        Mockito.verify(repository, Mockito.times(1)).save(updated);
    }

    @Test
    void updateUsername_FailExistingUsername() {
        String oldUsername = user.getUsername();
        String newUsername = "johnsmith";
        Mockito.when(repository.existsByUsername(newUsername)).thenReturn(true);

        Assertions.assertThrows(UserExistsException.class, () -> {
            service.updateUsername(oldUsername, newUsername);
        });
    }

    @Test
    void updateUsername_FailUserNotFound() {
        String oldUsername = user.getUsername();
        String newUsername = "johnsmith";
        Mockito.when(repository.findByUsername(oldUsername)).thenReturn(Optional.empty());

        Assertions.assertThrows(UserNotFoundException.class, () -> {
            service.updateUsername(oldUsername, newUsername);
        });
    }

    @Test
    void updatePassword_Success() {
        User updated = new User(1L, "johndoe", "John", "Doe", new Role("ROLE_USER"));
        String password = "password";
        Mockito.when(repository.findByUsername(updated.getUsername())).thenReturn(Optional.of(updated));

        service.updatePassword(updated.getUsername(), password);

        Assertions.assertEquals(updated.getPassword(), password);

        Mockito.verify(repository, Mockito.times(1)).save(updated);
    }

    @Test
    void updatePassword_FailUserNotFound() {
        Mockito.when(repository.findByUsername(user.getUsername())).thenReturn(Optional.empty());
        Assertions.assertThrows(UserNotFoundException.class, () -> {
            service.updatePassword(user.getUsername(), "");
        });
    }

}
