package com.gitlab.nikolapantelicftn.hybridp1.user.repositories;

import com.gitlab.nikolapantelicftn.hybridp1.user.exceptions.UserNotFoundException;
import com.gitlab.nikolapantelicftn.hybridp1.user.model.Role;
import com.gitlab.nikolapantelicftn.hybridp1.user.model.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class UserRepositoryTest {

    @Autowired
    private UserRepository repository;

    @Autowired
    private TestEntityManager entityManager;

    @Test
    public void findByUsername_Success() {
        User doe = new User("johndoe", "John", "Doe", "password", new Role("ROLE_USER"));
        entityManager.persist(doe);
        entityManager.flush();

        User found = repository.findByUsername("johndoe").get();

        Assertions.assertEquals(doe.getUsername(), found.getUsername());
    }

    @Test
    public void findByUsername_Fail() {
        Assertions.assertThrows(UserNotFoundException.class, () -> {
            repository.findByUsername("nonexistentusername").orElseThrow(() -> new UserNotFoundException("johndoe"));
        });
    }

    @Test
    public void existsByUsername_SuccessExists() {
        User wick = new User("johnwick", "John", "Wick", "password", new Role("ROLE_USER"));
        entityManager.persist(wick);
        entityManager.flush();

        boolean result = repository.existsByUsername(wick.getUsername());

        Assertions.assertTrue(result);
    }

    @Test
    public void existsByUsername_SuccessNotExists() {
        boolean result = repository.existsByUsername("nonexistentusername");

        Assertions.assertFalse(result);
    }

    @Test
    public void deleteByUsername_Success() {
        String username = "johncena";
        User cena = new User(username, "John", "Cena", "password", new Role("ROLE_USER"));
        User created = entityManager.persist(cena);
        entityManager.flush();

        repository.deleteByUsername(username);

        User result = entityManager.find(User.class, created.getId());

        Assertions.assertNull(result);

    }

}
