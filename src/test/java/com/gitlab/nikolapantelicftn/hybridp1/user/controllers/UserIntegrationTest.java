package com.gitlab.nikolapantelicftn.hybridp1.user.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.nikolapantelicftn.hybridp1.auth.model.AuthRequest;
import com.gitlab.nikolapantelicftn.hybridp1.user.controllers.dtos.NewUserDTO;
import com.gitlab.nikolapantelicftn.hybridp1.user.controllers.dtos.UserUpdateDTO;
import com.gitlab.nikolapantelicftn.hybridp1.user.model.Role;
import com.gitlab.nikolapantelicftn.hybridp1.user.model.User;
import com.gitlab.nikolapantelicftn.hybridp1.user.repositories.UserRepository;
import com.gitlab.nikolapantelicftn.hybridp1.util.TestSecurity;
import org.hamcrest.core.StringContains;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@SpringBootTest
@AutoConfigureMockMvc
public class UserIntegrationTest {

    @Autowired
    MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    UserRepository repository;

    private static String userHeader;
    private static String adminHeader;

    @BeforeEach
    void login() throws Exception {
        AuthRequest userLogin = new AuthRequest("user1", "pass");
        userHeader = TestSecurity.getAuthorizationHeader(mvc, userLogin);

        AuthRequest adminLogin = new AuthRequest("admin1", "pass");
        adminHeader = TestSecurity.getAuthorizationHeader(mvc, adminLogin);
    }

    @Test
    public void get_Success() throws Exception {
        final String username = "user1";
        User expected = repository.findByUsername("user1").orElseThrow();
        MvcResult result = mvc.perform(get("/api/users/" + username)
                .contentType(MediaType.APPLICATION_JSON).header(HttpHeaders.AUTHORIZATION, adminHeader))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn();

        String resultJson = result.getResponse().getContentAsString();
        User userResult = objectMapper.readValue(resultJson, User.class);

        Assertions.assertEquals(expected, userResult);

    }

    @Test
    public void get_NotFound() throws Exception {
        final String username = "nonexistentusername";
        mvc.perform(get("/api/users/" + username).header(HttpHeaders.AUTHORIZATION, adminHeader))
                .andExpect(status().isNotFound());
    }

    @Test
    public void getAll_Success() throws Exception {
        List<User> expected = repository.findAll();
        MvcResult result = mvc.perform(get("/api/users")
                .contentType(MediaType.APPLICATION_JSON).header(HttpHeaders.AUTHORIZATION, adminHeader))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andReturn();

        String resultJson = result.getResponse().getContentAsString();
        List<User> usersResult = List.of(objectMapper.readValue(resultJson, User[].class));

        assertThat(usersResult).hasSameElementsAs(expected);
    }

    @Test
    public void create_Success() throws Exception {
        NewUserDTO newUser = new NewUserDTO("johndoe", "password", "John", "Doe");
        String userJson = objectMapper.writeValueAsString(newUser);

        User expected = modelMapper.map(newUser, User.class);

        MvcResult result = mvc.perform(post("/api/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(userJson).header(HttpHeaders.AUTHORIZATION, adminHeader))
                .andExpect(status().isOk())
                .andReturn();

        String resultJson = result.getResponse().getContentAsString();
        User userResult = objectMapper.readValue(resultJson, User.class);

        expected.setId(userResult.getId());
        expected.setRole(userResult.getRole());

        Assertions.assertEquals(expected, userResult);
    }

    @Test
    public void create_ValidationError() throws Exception {
        NewUserDTO newUser = new NewUserDTO("", "", "", "");
        String userJson = objectMapper.writeValueAsString(newUser);

        mvc.perform(post("/api/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(userJson).header(HttpHeaders.AUTHORIZATION, adminHeader))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(StringContains.containsString("username must not be blank")))
                .andExpect(content().string(StringContains.containsString("password must not be blank")))
                .andExpect(content().string(StringContains.containsString("firstName must not be blank")))
                .andExpect(content().string(StringContains.containsString("lastName must not be blank")));

    }

    @Test
    public void update_Success() throws Exception {
        final Long id = 1L;
        UserUpdateDTO userUpdate = new UserUpdateDTO("user1", "John", "Cena");

        String userJson = objectMapper.writeValueAsString(userUpdate);
        User expected = modelMapper.map(userUpdate, User.class);
        expected.setId(id);

        MvcResult result = mvc.perform(put("/api/users/" + id)
                .contentType(MediaType.APPLICATION_JSON)
                .content(userJson).header(HttpHeaders.AUTHORIZATION, adminHeader))
                .andExpect(status().isOk())
                .andReturn();

        String resultJson = result.getResponse().getContentAsString();
        User userResult = objectMapper.readValue(resultJson, User.class);

        expected.setRole(userResult.getRole());

        Assertions.assertEquals(expected, userResult);
    }

    @Test
    public void update_NotFound() throws Exception {
        final Long id = 1000L;
        UserUpdateDTO userUpdate = new UserUpdateDTO("user1", "John", "Cena");
        String userJson = objectMapper.writeValueAsString(userUpdate);

        mvc.perform(put("/api/users/" + id)
                .contentType(MediaType.APPLICATION_JSON)
                .content(userJson).header(HttpHeaders.AUTHORIZATION, adminHeader))
                .andExpect(status().isNotFound());

    }

    @Test
    public void update_ValidationError() throws Exception {
        UserUpdateDTO userUpdate = new UserUpdateDTO("", "", "");
        String userJson = objectMapper.writeValueAsString(userUpdate);

        mvc.perform(put("/api/users/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(userJson).header(HttpHeaders.AUTHORIZATION, adminHeader))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(StringContains.containsString("username must not be blank")))
                .andExpect(content().string(StringContains.containsString("firstName must not be blank")))
                .andExpect(content().string(StringContains.containsString("lastName must not be blank")));

    }

    @Test
    public void delete_Success() throws Exception {
        User newUser = new User("fordelete", "John", "Doe", "password", new Role("ROLE_USER"));
        repository.save(newUser);
        final String username = "fordelete";
        mvc.perform(delete("/api/users/" + username).header(HttpHeaders.AUTHORIZATION, adminHeader))
                .andExpect(status().isOk());
    }

    @Test
    public void delete_FailNotFound() throws Exception {
        final String username = "nonexistentusername";
        mvc.perform(delete("/api/users/" + username).header(HttpHeaders.AUTHORIZATION, adminHeader))
                .andExpect(status().isNotFound());
    }

}
